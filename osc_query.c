/************************************************************************/
/* Copyright (C) 2011 Lieven Moors                                      */
/*                                                                      */
/* This file is part of osc_query.                                      */
/*                                                                      */
/* osc_query is free software: you can redistribute it and/or modify    */
/* it under the terms of the GNU General Public License as published by */
/* the Free Software Foundation, either version 3 of the License, or    */
/* (at your option) any later version.                                  */
/*                                                                      */
/* osc_query is distributed in the hope that it will be useful,         */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of       */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        */
/* GNU General Public License for more details.                         */
/*                                                                      */
/* You should have received a copy of the GNU General Public License    */
/* along with osc_query. If not, see <http://www.gnu.org/licenses/>.    */
/************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <lo/lo.h>

lo_address *address = NULL;
unsigned int replies_2 = 0;

static void usage(void)
{
    fprintf(stdout, "Usage: osc_query -p [port]\n");
}

void error_handler(int num, const char *msg, const char *where)
{
    fprintf(stderr, "errno:%i\nmsg:%s\nwhere:%s\n", num, msg, where);
}

int query_paths(const char *path, const char *types, lo_arg **argv, int argc, lo_message msg, void *user_data)
{
    replies_2++;
    char *qp;
    int base_len = strlen(&argv[0]->s);

    if(argc == 1){ // Is terminal node.
        qp = (char*)malloc(base_len + 1); // one extra for slash
        strcpy(qp, &argv[0]->s);
        fprintf(stdout, "%s\n", qp);
    }

    if(argc > 1){ // More nodes are following.
        unsigned int i;
        for(i = 1; i < argc; i++){
            int len = strlen(&argv[i]->s);
            qp = (char*)malloc(base_len + len + 2); // one extra for slash
            strcpy(qp, &argv[0]->s);
            strcat(qp, &argv[i]->s);
            strcat(qp, "/");
            if(lo_send(address, qp, "") < 0)
                fprintf(stderr, "Error: could not send message.\n");
        }
    }
    free(qp);
}

static const struct option long_option[] = {
        {"port", 1, NULL, 'p'},
};


int main(int argc, char **argv)
{
    const char* port = NULL;
    const char* qp = "/";

    unsigned int replies_1 = 0;
    unsigned int sleep_time = 200;
    unsigned int retries = 100;
    unsigned int retries_done = 0;

    int c;
    while ((c = getopt_long(argc, argv, "p:", long_option, NULL)) != -1){
        switch (c){
            case 'p':
                port = optarg;
                break;
            default:
                usage();
                exit(EXIT_FAILURE);
        }
    }

    if (argc > optind) {
        usage();
        exit(EXIT_FAILURE);
    }

    address = lo_address_new(NULL, port);

    lo_server_thread server_thread  = lo_server_thread_new(NULL, error_handler);
    lo_method method = lo_server_thread_add_method(server_thread, NULL, NULL, query_paths, NULL);

    if(lo_server_thread_start(server_thread) != 0) 
        fprintf(stderr, "Error: could not start server thread.");

    if(lo_send(address, qp, "") < 0) // Send one message to start it off.
        fprintf(stderr, "Error: could not send message.\n");

    while(1){
        usleep(sleep_time);
        if(replies_2 == replies_1){
            if(retries_done++ >= retries) break;
        } else {
            replies_1 = replies_2;
            retries_done = 0;
        }
    }
    return EXIT_SUCCESS;
}
